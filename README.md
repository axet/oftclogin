# Pidgin OFTC Login

This plugin was developed to automatically login to oftc.net. oftc has non standart login sequince and pidgin faild to login to the network. You can add Pidgin "Add Buddy Pounce" and send credentials to NickServ, but that would show conversation window every time you reconnect.

This plugin allow to identify silently.

## Links

* https://issues.imfreedom.org/issue/PIDGIN-16291/Can-not-autoidentify-on-irc.oftc.net
* https://developer.pidgin.im/wiki/Protocol%20Specific%20Questions#HowdoIsendamessagetoauserorbotonlogin

## USAGE

Once you have the plugin installed, activate it (Tools -> Plugins).

The plugin works automatically with no configuration.
