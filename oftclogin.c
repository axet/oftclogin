#include <string.h>

#include <account.h>
#include <accountopt.h>
#include <cmds.h>
#include <connection.h>
#include <conversation.h>
#include <debug.h>
#include <notify.h>
#include <plugin.h>
#include <pluginpref.h>
#include <prefs.h>
#include <util.h>

#define PLUGIN_STATIC_NAME "oftclogin"
#define PLUGIN_ID "core-rlaager-" PLUGIN_STATIC_NAME
#define PLUGIN_AUTHOR "Alexey Kuznetsov <axet@me.com>"

#define IRC_PLUGIN_ID "prpl-irc"

#define PP_VERSION "0.0.1"
#define PP_WEBSITE "https://gitlab.com/axet/ircsuppress"
#define _(x) x

/*****************************************************************************
 * Prototypes                                                                *
 *****************************************************************************/

static gboolean plugin_load(PurplePlugin *plugin);
static gboolean plugin_unload(PurplePlugin *plugin);

/*****************************************************************************
 * Plugin Info                                                               *
 *****************************************************************************/

static PurplePluginInfo info =
{
  PURPLE_PLUGIN_MAGIC,
  2,
  0,
  PURPLE_PLUGIN_STANDARD,          /**< type           */
  NULL,                            /**< ui_requirement */
  0,                               /**< flags          */
  NULL,                            /**< dependencies   */
  PURPLE_PRIORITY_DEFAULT,         /**< priority       */
  PLUGIN_ID,                       /**< id             */
  NULL,                            /**< name           */
  PP_VERSION,                      /**< version        */
  NULL,                            /**< summary        */
  NULL,                            /**< description    */
  PLUGIN_AUTHOR,                   /**< author         */
  PP_WEBSITE,                      /**< homepage       */
  plugin_load,                     /**< load           */
  plugin_unload,                   /**< unload         */
  NULL,                            /**< destroy        */

  NULL,                            /**< ui_info        */
  NULL,                            /**< extra_info     */
  NULL,                            /**< prefs_info     */
  NULL,                            /**< actions        */
  NULL,                            /**< reserved 1     */
  NULL,                            /**< reserved 2     */
  NULL,                            /**< reserved 3     */
  NULL                             /**< reserved 4     */
};

static gboolean receiving_im_msg_cb(PurpleAccount *account, gchar **sender,
                                    gchar **buffer,
                                    PurpleConversation *conv,
                                    gint *flags, gpointer data)
{
  gchar *msg;
  gchar *nick;
  PurpleConnection *connection;

  if (!g_str_equal(purple_account_get_protocol_id(account), IRC_PLUGIN_ID))
    return FALSE;
  
  if (conv != 0)
    return FALSE;

  msg = *buffer;
  nick = *sender;

  connection = purple_account_get_connection(account);
  g_return_val_if_fail(NULL != connection, FALSE);

  const char *name = purple_account_get_username(account);

  if (!g_str_equal(nick, "NickServ"))
    return FALSE;

  if (g_strstr_len(msg, -1, "This nickname is registered and protected") == 0)
    return FALSE;
    
  if (!g_str_has_suffix(name, "oftc.net"))
    return FALSE;

  PurplePlugin *irc_prpl;
  PurplePluginProtocolInfo *prpl_info;

  irc_prpl = purple_plugins_find_with_id(IRC_PLUGIN_ID);

  if (NULL == irc_prpl)
    return FALSE;

  prpl_info = PURPLE_PLUGIN_PROTOCOL_INFO(irc_prpl);
  
  const char *pass = purple_account_get_password(account);
  char *cmd = g_strconcat("id ", pass, NULL);
  purple_debug_info("oftclogin", "send credentials to NickServ %s\n", nick);
  prpl_info->send_im(connection, nick, cmd, 0);
  g_free(cmd);

  return FALSE;
}

static gboolean plugin_load(PurplePlugin *plugin)
{
  PurplePlugin *irc_prpl;
  PurplePluginProtocolInfo *prpl_info;
  void *conv_handle;

  irc_prpl = purple_plugins_find_with_id(IRC_PLUGIN_ID);

  if (NULL == irc_prpl)
    return FALSE;

  prpl_info = PURPLE_PLUGIN_PROTOCOL_INFO(irc_prpl);
  if (NULL == prpl_info)
    return FALSE;

  conv_handle = purple_conversations_get_handle();
  purple_signal_connect(conv_handle, "receiving-im-msg",
                        plugin, PURPLE_CALLBACK(receiving_im_msg_cb),
                        NULL);
  return TRUE;
}

static gboolean plugin_unload(PurplePlugin *plugin)
{
  PurplePlugin *irc_prpl;
  PurplePluginProtocolInfo *prpl_info;
  GList *list;

  irc_prpl = purple_plugins_find_with_id(IRC_PLUGIN_ID);
  if (NULL == irc_prpl)
    return FALSE;

  prpl_info = PURPLE_PLUGIN_PROTOCOL_INFO(irc_prpl);
  if (NULL == prpl_info)
    return FALSE;

  list = prpl_info->protocol_options;

  /* Remove protocol preferences. */
  while (NULL != list)
  {
    PurpleAccountOption *option = (PurpleAccountOption *) list->data;

    if (g_str_has_prefix(purple_account_option_get_setting(option), PLUGIN_ID "_"))
    {
      GList *llist = list;

      /* Remove this element from the list. */
      if (llist->prev)
        llist->prev->next = llist->next;
      if (llist->next)
        llist->next->prev = llist->prev;

      purple_account_option_destroy(option);

      list = g_list_next(list);

      g_list_free_1(llist);
    }
    else
      list = g_list_next(list);
  }

  return TRUE;
}

static void plugin_init(PurplePlugin *plugin)
{
  info.dependencies = g_list_append(info.dependencies, IRC_PLUGIN_ID);

#ifdef ENABLE_NLS
  bindtextdomain(GETTEXT_PACKAGE, PP_LOCALEDIR);
  bind_textdomain_codeset(GETTEXT_PACKAGE, "UTF-8");
#endif /* ENABLE_NLS */

  info.name = _("OFTC Login");
  info.summary = _("Handles login with oftc.net server.");
  info.description = _("- OFTC has custom login system, this plugin send your password to NickServ");
}

PURPLE_INIT_PLUGIN(PLUGIN_STATIC_NAME, plugin_init, info)
